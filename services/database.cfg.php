<?php
/**
 * JIMBS (Jabber Instant Messaging Bot Services)
 * Copyright (C) 2010  Martin Kelm
 * This file is part of JIMB.
 *
 * JIMB is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * JIMB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JIMB; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @file       Database config
 * @package    JIMB
 * @subpackage Services 
 * @author     Martin Kelm <martinkelm@idxsolutions.de>
 * @copyright  2010 Martin Kelm
 */

$databaseCfg = array(
  'host' => 'localhost',
  'user' => 'cjimb',
  'pass' => 'hello23',
  'db' => 'cjimb'
);

?>
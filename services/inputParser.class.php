<?php
/**
 * JIMBS (Jabber Instant Messaging Bot Services)
 * Copyright (C) 2010  Martin Kelm
 * This file is part of JIMB.
 *
 * JIMB is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * JIMB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JIMB; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @file       Input parser class
 * @package    JIMB
 * @subpackage Services 
 * @author     Martin Kelm <martinkelm@idxsolutions.de>
 * @copyright  2010 Martin Kelm
 */

class inputParser {

  private $_userHash = NULL;

  /**
   * This variable stores some data of user data,
   * i.e. to support short commands in a related module context.
   * @var array $userData
   */
  private $_userData = NULL;
  
  private $_modulesHandles = NULL;

  public function __construct() {
    $this->_userData = array();
    $this->_modulesHandles = array(
      'dates' => array('date', 'dts'),
      'reminders' => array('reminder', 'rmd'),
      'dictionary' => array('dict', 'dct')
    );
  }

  private function initUserData($jabberIdRes) {
    $this->_userHash = md5($jabberIdRes);
    if (empty($this->_userData[$this->_userHash])) {
      $this->_userData[$this->_userHash] = array(
        'module' => NULL
      );
    }
    if (isset($this->_userData[$this->_userHash])) {
      return TRUE;
    }
    return FALSE;
  }

  private function getUserData($fieldName) {
    if (isset($this->_userData[$this->_userHash]) &&
        isset($this->_userData[$this->_userHash][$fieldName])) {
      return $this->_userData[$this->_userHash][$fieldName];
    }
    return NULL;
  }

  private function setUserData($fieldName, $fieldValue) {
    if (isset($this->_userData[$this->_userHash])) {
      $this->_userData[$this->_userHash][$fieldName] = $fieldValue;
      return TRUE;
    }
    return FALSE;
  }

  public function parseInput(&$str, $jabberIdRes) {
    $str = $str." "; // add a final split marker
    $strLen = strlen($str);
    $tmpStr = ""; // temporary string
    $validCommand = FALSE; // checks command validity
    $splits = 0; // count splits to control command sequence
    $paramName = NULL; // store param names

    $result = array('module' => NULL, 'command' => NULL, 'params' => array());

    $this->initUserData($jabberIdRes);

    for ($i = 0; $i < $strLen; $i++) {
      $curStr = substr($str, $i, 1);
      if ($i+1 < $strLen) {
        $nextStr = substr($str, $i+1, 1);
      } else {
        $nextStr = NULL;
      }

      if ($i == 0 && $curStr == "!") {
        $validCommand = TRUE; // validate
        $continue = FALSE;

      } elseif ($i == 0) {
        // get last module if no module has been specified
        $result['module'] = $this->getUserData('module');
        if (!empty($result['module'])) {
          $validCommand = TRUE;
        }
        $continue = TRUE;

      } else {
        $continue = TRUE;
      }

      if ($continue == TRUE && $validCommand == TRUE) {

        // no split, just add a letter
        if ($curStr != " " ||
            ($paramName !== NULL && ($nextStr !== '-' && $nextStr !== NULL))) {
          $tmpStr .= $curStr;

        } elseif ($result['module'] == NULL) { // split, detect destination module

          if ($splits == 0) {
            /* check if the main module handle is available
             * to use it as module identifier
             */
            if (isset($this->_modulesHandles[$tmpStr])) {
              // set it right here
              $result['module'] = $tmpStr;
            } else {
              // or use a short or alternative module handle
              foreach ($this->_modulesHandles as $handle => $altHandles) {
                if (in_array($tmpStr, $altHandles)) {
                  $result['module'] = $handle;
                }
              }
            }
          }

          $tmpStr = "";
          $splits++;

        } elseif ($result['command'] == NULL) { // split, without command yet

          // set command
          $result['command'] = $tmpStr;

          $tmpStr = "";
          $splits++;

        } else { // split for parameters and parameter values

          if (substr($tmpStr, 0, 1) == "-") { // detect parameter name
            $paramName = substr($tmpStr, 1);

          } elseif ($paramName != NULL && $paramName != $tmpStr) {
             // add result parameter with value
             $result['params'][$paramName] = $tmpStr;
             $paramName = NULL;
          } else {
            $result['params'][] = $tmpStr;
            $paramName = NULL;
          }

          $tmpStr = "";
          $splits++;

        }
      }
    }

    $this->setUserData(
      'module', (!empty($result['module'])) ? $result['module'] : NULL
    );

    return $result;
  }

}

?>

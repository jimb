<?php
/**
 * JIMBS (Jabber Instant Messaging Bot Services)
 * Copyright (C) 2010  Martin Kelm
 * This file is part of JIMB.
 *
 * JIMB is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * JIMB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JIMB; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @file       Database class
 * @package    JIMB
 * @subpackage Services 
 * @author     Martin Kelm <martinkelm@idxsolutions.de>
 * @copyright  2010 Martin Kelm
 */

class database {

  /**
  * Mysqi object
  * @var mysqli
  */
  private $_mysqli = NULL;

  /**
  * Mysqli data
  * @var array
  */
  private $_connectionData = array();

  /**
  * Current connection index value.
  * @var integer
  */
  private $_connectionIndex = -1;

  /**
  * Current set of established database connections
  * @var array
  */
  private $_connection = array();

  /**
   * Disabled construct method, use get instance instead!
   *
   * @see database::getInstance
   */
  protected function __construct() { }

  /**
   * Disabled clone method, use get instance instead!
   *
   * @see database::getInstance
   */
  protected function __clone() { }

  /**
  * Gets an object instance.
  *
  * You can use a static instance to save memory for additional objects and
  * to use a single set of connections in different contexts.
  *
  * @param boolean $static Use a static object to use a single instance only.
  * @return database $instance Returns a (non-)static object instance.
  */
  public static function getInstance($static = TRUE) {
    if ($static === TRUE) {
      static $instance = NULL;
    } else {
      $instance = NULL;
    }
    if (!(is_object($instance) && $instance instanceof database)) {
      $instance = new database();
    }
    return (is_object($instance) && $instance instanceof database) ?
      $instance : NULL;
  }

  /**
  * Sets a hostname, a username, a password and a database name.
  *
  * These data are required to setup a mysqli connection later.
  * The input data are going to be checked against a valid type and content.
  *
  * @param string $hostname Database server host name.
  * @param string $username Database user name.
  * @param string $password Database password.
  * @param string $database Database name.
  * @throws InvalidArgumentException Exception if an invalid type has been set.
  * @throws UnexpectedValueException Exception if an empty parameter has been set.
  */
  public function setConnectionData($hostname, $username, $password, $database) {
    if (!is_string($hostname)) {
      $invalidParam = 'hostname';
    } elseif (!is_string($username)) {
      $invalidParam = 'username';
    } elseif (!is_string($password)) {
      $invalidParam = 'password';
    } elseif (!is_string($database)) {
      $invalidParam = 'database';
    }
    if (!empty($invalidParam)) {
      throw new InvalidArgumentException(
        sprintf('Set a valid %s type!', $invalidParam)
      );
    }
    if (empty($hostname)) {
      $emptyParam = 'hostname';
    } elseif (empty($username)) {
      $emptyParam = 'username';
    } elseif (empty($password)) {
      $emptyParam = 'password';
    } elseif (empty($database)) {
      $emptyParam = 'database';
    }
    if (!empty($emptyParam)) {
      throw new UnexpectedValueException(
        sprintf('Set a valid %s value!', $emptyParam)
      );
    }
    $this->_connectionData = array(
      'hostname' => $hostname,
      'username' => $username,
      'password' => $password,
      'database' => $database
    );
  }

  /**
  * Sets a custom mysqli connection object.
  *
  * A connection has to be initialized before setting an object in here.
  *
  * @param mysqli $mysqli
  */
  public function setMysqliObject($mysqli) {
    $this->_mysqli = $mysqli;
  }

  /**
  * Gets a mysqli connection object by using the current mysqli object or data.
  *
  * The mysqli constructor will drop an error if a connection problem occurs.
  *
  * @throws UnexpectedValueException Exception if no mysql data have been set.
  * @return mysqli Returns a mysqli connection object.
  */
  protected function getMysqliObject() {
    if (is_object($this->_mysqli) && $this->_mysqli instanceof mysqli) {
      return $this->_mysqli;
    }
    if (!empty($this->_connectionData)) {
      try {
        return new mysqli(
          $this->_connectionData['hostname'],
          $this->_connectionData['username'],
          $this->_connectionData['password'],
          $this->_connectionData['database']
        );
      } catch (Exception $e) {
        throw new LogicException('Cannot get mysql connection object!');
      }
    } else {
      throw new UnexpectedValueException('Set connection data first!');
    }
  }

  /**
  * Connect by connection index.
  *
  * @param integer $index Connection by index value to connect to.
  * @return boolean Returns TRUE on success.
  */
  public function connect($index = 0) {
    if (!is_integer($index)) {
      throw new InvalidArgumentException('Set a valid index type!');
    }
    if ($index < 0) {
      throw new OutOfRangeException('Set a positive index value!');
    }
    $this->_connection[$index] = $this->getMysqliObject();
    $this->_connectionIndex = $index;
    return TRUE;
  }

  /**
  * Close connection by connection index.
  *
  * @param integer $index Connection by index value to close.
  * @return boolean Returns TRUE on success.
  */
  public function close($index = 0) {
    if (!is_integer($index)) {
      throw new InvalidArgumentException('Set a valid index type!');
    }
    if ($index < 0 || !isset($this->_connection[$index])) {
      throw new OutOfRangeException('Set a positive and valid index value!');
    }
    unset($this->_connection[$index]);
    $this->_connectionIndex = -1;
    return TRUE;
  }

  /**
  * Executes a simple query like INSERT, UPDATE or DELETE with placemark values.
  * The placemark values have different types:
  * - i = corresponding variable has type integer
  * - d = corresponding variable has type double
  * - s = corresponding variable has type string
  * - b = corresponding variable is a blob and will be sent in packets
  *
  * @param string $sql Contains sql statement with optional ?-Placemarks.
  * @param string $params Contains placemarks values with type (name => type)
  * @return boolean $result Executed?
  */
  public function query($sql) {
    return $this->_connection[$this->_connectionIndex]->query($sql);
  }
}

?>

<?php
/**
 * JIMBS (Jabber Instant Messaging Bot Services)
 * Copyright (C) 2010  Martin Kelm
 * This file is part of JIMB.
 *
 * JIMB is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * JIMB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JIMB; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @file       Module abstraction class
 * @package    JIMB
 * @subpackage Services 
 * @author     Martin Kelm <martinkelm@idxsolutions.de>
 * @copyright  2010 Martin Kelm
 */

abstract class modBase {

  protected function getInput(&$input, $keys) {
    if (is_array($input) && count($input) > 0) {
      foreach ($keys as &$key) {
        if (isset($input[$key])) {
          return $input[$key];
        }
      }
    } elseif (in_array($input, $keys)) {
      return $input;
    }
    return NULL;
  }

  protected function getJabberId($jabberIdRes) {
    if (!empty($jabberIdRes)) {
      $jidParts = explode("/", $jabberIdRes); // split jabber id and ressource
      unset($jabberIdRes);
      if (!empty($jidParts[0])) {
        return $jidParts[0];
      }
    }
    return NULL;
  }

  /**
   * Basic method to add and perform module actions.
   *
   * @param reference $command Array with command data
   * @param string $from Source Jabber ID / Ressource
   * @return string Return message
   */
  public function &perform(&$parsedInput, $jabberIdRes) {
    // default actions
    $result = NULL;
    return $result;
  }

  /**
   * Basic method to add and perform module updates.
   *
   * @param array Return messages containing jid and text
   */
  public function update(&$msgs) {
    // updates
  }

}

?>

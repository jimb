<?php
/**
 * JIMBS (Jabber Instant Messaging Bot Services)
 * Copyright (C) 2010  Martin Kelm
 * This file is part of JIMB.
 *
 * JIMB is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * JIMB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JIMB; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @file       Dictionary module
 * @package    JIMB
 * @subpackage Services
 * @author     Martin Kelm <martinkelm@idxsolutions.de>
 * @copyright  2010 Martin Kelm
 */
require_once(dirname(__FILE__).'/modBase.php');

class modDictionary extends modBase {

  public function __construct() {
  }

  public function &perform(&$parsedInput, $jabberIdRes) {
    $result = NULL;

    if (!empty($parsedInput['command']) && !empty($parsedInput['params'])) {

      if (NULL !== $this->getInput($parsedInput['command'], array('english', 'en'))) {
        $lang = 'en';
      }
      if (NULL !== $this->getInput($parsedInput['command'], array('french', 'fr'))) {
        $lang = 'fr';
      }
      if (NULL !== $this->getInput($parsedInput['command'], array('french', 'es'))) {
        $lang = 'es';
      }
      if (NULL !== $this->getInput($parsedInput['command'], array('italian', 'it'))) {
        $lang = 'it';
      }

      switch ($lang) {
      case 'en':
      case 'fr':
      case 'es':
      case 'it':
        $search = $this->getInput($parsedInput['params'], array('search', 's', 0));
        $result = $this->translate($lang, $search);
        break;
      }
    }

    return $result;
  }

  protected function translate($lang, $search) {

    if (!empty($lang) && !empty($search)) {
      $translations = $this->getTranslations($search, $lang);
      if (count($translations) > 0) {
        if (count($translations) > 5) {
          $max = 5;
        } else {
          $max = count($translations);
        }
        $msg = "These translations might help you:";
        for ($i = 0; $i < $max; $i++) {
          $msg .= "\n".$translations[$i];
        }
      }
    }

    if (isset($msg)) {
      return $msg;
    } else {
      return 'No translations have been found!';
    }
  }

  protected function getTranslations($searchBy, $lang) {
    $results = array();

    // lang = en / fr / es / it / ch
    $targetHost = "dict.leo.org";
    $targetFile = "/".$lang."de?search=".urlencode($searchBy);
    $errno = 0;
    $errstr = "";

    // open request socken
    if ($tfh = @fsockopen($targetHost, 80, $errno, $errstr, 3)) {
      // put client request header
      fputs($tfh, "GET ".$targetFile." HTTP/1.0\r\n");
      fputs($tfh, "Host: ".$targetHost."\r\n");
      fputs($tfh, "Referer: http://".$targetHost."\r\n");
      fputs($tfh, "User-Agent: Mozilla/5.0 (X11; U; Linux x86_64; ".
        "de; rv:1.8.1.2) Gecko/20060601 Firefox/2.0.0.2 (Ubuntu-edgy)\r\n\r\n");

      // load response content and close request socket
      $response = "";
      while (is_resource($tfh) && $tfh && !feof($tfh)) {
        $response .= fread($tfh, 1024);
      }
      fclose($tfh);

      // check if response is ok
      if (preg_match("/^HTTP\/1\.[0-9] 200 OK/", $response)) {
        // get data
        $response = str_replace(array("\n", "\r"), "", $response);

        if (preg_match_all("/<table cellpadding=0 cellspacing=0 width=\"100%\" id=\"results\" border=1>(.*)<\/table>/U", $response, $match)) {

          // get full table with words
          if (isset($match[1][0])) {
            $content = $match[1][0];
            unset($match);
            preg_match_all("/<tr(.*)<\/tr>/U", $content, $match);

            // get rows with word
            if (isset($match[1]) && count(isset($match[1])) > 0) {
              $content = $match[1];
              unset($match);
             
              foreach ($content as &$rowContent) {
                preg_match_all("/<td valign=\"middle\" width=\"[0-9]*%\">(.*)<\/td>/U", $rowContent, $match);
                // get table cells with words
                if (!empty($match[1]) && count($match[1]) > 0) {
                  $str = $match[1][0]." = ".$match[1][1];
                  $str = preg_replace("/<.*>/U", "", $str); // remove html elements
                  $str = str_replace("'", "\'", $str);
                  $str = utf8_encode($str);
                  $results[] = $str;
                }
                unset($match);
                unset($str);
              }
            }
          }
        }
      }
      unset($response);
    }

    return $results;
  }
}

?>
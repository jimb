<?php
/**
 * JIMBS (Jabber Instant Messaging Bot Services)
 * Copyright (C) 2010  Martin Kelm
 * This file is part of JIMB.
 *
 * JIMB is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * JIMB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JIMB; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @file       Dates module
 * @package    JIMB
 * @subpackage Services
 * @author     Martin Kelm <martinkelm@idxsolutions.de>
 * @copyright  2010 Martin Kelm
 */
require_once(dirname(__FILE__).'/modBase.php');

class modDates extends modBase {

  private $db = NULL;

  private $dbTable = NULL;

  public function __construct($db) {
    $this->db = $db;
    $this->dbTable = "mod_dates";
  }

  public function &perform(&$parsedInput, $jabberIdRes) {
    $result = NULL;

    $jabberId = $this->getJabberId($jabberIdRes);
    if (!empty($parsedInput['command']) && !empty($jabberId)) {

      switch ($parsedInput['command']) {
      case 'add':
        // convert short params
        $date = $this->getInput($parsedInput['params'], array('date', 'd', 0));
        $time = $this->getInput($parsedInput['params'], array('time', 't', 1));
        if (!empty($time)) {
          $note = $this->getInput($parsedInput['params'], array('note', 'n', 2));
        } else {
          $note = $this->getInput($parsedInput['params'], array('note', 'n', 1));
        }
        // get result string
        $result = $this->addDate($jabberId, $note, $date, $time);
        break;
      case 'show':
        $page = $this->getInput($parsedInput['params'], array('page', 'p', 0));
        $result = $this->showDates($jabberId, $page);
        break;
      case 'remove':
      case 'rem':
        $id = $this->getInput($parsedInput['params'], array('id', 'i', 0));
        $result = $this->removeDate($jabberId, $id);
        break;
      }
    }

    return $result;
  }

  protected function addDate($jabberId, $note, $date, $time = NULL) {
    $added = FALSE;

    if (!empty($jabberId) && !empty($note)) {

      if (empty($time)) {
        $time = "00:00:00";
      } elseif (strlen($time) < 8) {
        $offset = strlen($time);
        $time = $time.substr("00:00:00", $offset);
      } else {
        $time = $time;
      }

      $dateParts = preg_split('~[-: ]~', $date." ".$time);
      $timestamp = mktime(
        (int)$dateParts[3], (int)$dateParts[4], (int)$dateParts[5],
        (int)$dateParts[1], (int)$dateParts[2], (int)$dateParts[0]
      );

      $sql = sprintf(
        "SELECT COUNT(*) AS amount
           FROM %s
          WHERE date_jid = '%s'
            AND date_time = '%d'
            AND date_note = '%s'",
        $this->dbTable, $jabberId, $timestamp, $note
      );

      if ($res = $this->db->query($sql)) {
        $result = $res->fetch_assoc();
        if (empty($result['amount'])) {
          $sqlParams = array(
            'date_jid' => $jabberId,
            'date_time' => $timestamp,
            'date_note' => $note,
            'date_past' => 0
          );
          $sql = sprintf(
            "INSERT INTO %s (%s) 
             VALUES ('%s')",
            $this->dbTable,
            implode(",", array_keys($sqlParams)),
            implode("','", array_values($sqlParams))
          );
          $added = FALSE !== $this->db->query($sql);
        }
      }
    }

    if ($added == TRUE) {
      return 'Your date has been added!';
    } else {
      return 'Error, your date has not been added!';
    }
  }

  protected function showDates($jabberId, $page, $limit = 5) {

    if (!empty($jabberId)) {

      $sql = sprintf(
        "SELECT COUNT(*) AS amount
           FROM %s
          WHERE date_jid = '%s'
            AND date_past = '0'",
        $this->dbTable,
        $jabberId
      );
      if ($res = $this->db->query($sql)) {
        $result = $res->fetch_assoc();
        if (!empty($result['amount'])) {

          $maxPage = ceil($count/$limit);
          if (isset($page) && $page > 0 && $page <= $maxPage) {
            $offset = $page-1; // get offset by params
          } else {
            $offset = 0;
          }

          $sql = sprintf(
            "SELECT date_id, date_time, date_note
              FROM %s
             WHERE date_jid = '%s'
               AND date_past = '0'
             ORDER BY date_time
             LIMIT %d, %d",
            $this->dbTable,
            $jabberId,
            $offset*$limit,
            $limit
          );
          if ($res = $this->db->query($sql)) {
            $msg = "Your dates are:";
            while ($row = $res->fetch_assoc()) {
              $msg .= sprintf("\n%s: %s (ID %d)",
                date("Y-m-d, H:i:s", $row['date_time']),
                $row['date_note'], $row['date_id']
              );
            }
            if ($maxPage > 1) {
              $msg .= sprintf(
                "\nPage %d of %d, use -page X to change page offset.",
                $offset+1, $maxPage
              );
            }
          }
        } else {
          $msg = "You do not have any dates yet.";
        }
      }
    }

    if (!empty($msg)) {
      return $msg;
    } else {
      return 'Error, no dates are available!';
    }
  }

  protected function removeDate($jabberId, $id) {
    $removed = FALSE;

    if (!empty($jabberId) && !empty($id)) {
      $sql = sprintf(
        "DELETE FROM %s 
          WHERE date_id = '%d'
            AND date_jid = '%s'",
        $this->dbTable,
        $id,
        $jabberId
      );
      
      $removed = FALSE !== $this->db->query($sql);
    }

    if ($removed == TRUE) {
      return 'Your date has been removed!';
    } else {
      return 'Error, your date has not been removed!';
    }
  }

  public function update(&$msgs) {
    // move old dates to vault
    $sql = sprintf(
      "UPDATE %s 
          SET date_past = 1 
        WHERE date_time < '%d'",
      $this->dbTable,
      time()
    );
    $this->db->query($sql);
  }

}

?>

<?php
/**
 * JIMBS (Jabber Instant Messaging Bot Services)
 * Copyright (C) 2010  Martin Kelm
 * This file is part of JIMB.
 *
 * JIMB is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * JIMB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JIMB; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @file       Reminders module
 * @package    JIMB
 * @subpackage Services
 * @author     Martin Kelm <martinkelm@idxsolutions.de>
 * @copyright  2010 Martin Kelm
 */
require_once(dirname(__FILE__).'/modBase.php');

class modReminders extends modBase {

  private $db = NULL;

  private $dbTableDates = NULL;
  private $dbTableReminders = NULL;
  private $dbTableRemindersDates = NULL;

  private $reminderTypes = NULL;

  public function __construct($db) {
    $this->db = $db;
    
    $this->dbTableDates = "mod_dates";
    $this->dbTableReminders = "mod_reminders";
    $this->dbTableRemindersDates = "mod_reminders_dates";

    $this->reminderTypes = array("weeks", "days", "hours", "minutes");
  }

  public function &perform(&$parsedInput, $jabberIdRes) {
    $result = NULL;

    $jabberId = $this->getJabberId($jabberIdRes);
    if (!empty($parsedInput['command']) && !empty($jabberId)) {

      switch ($parsedInput['command']) {
      case 'add':
        // convert short params
        $type = $this->getInput($parsedInput['params'], array('type', 't', 0));
        $value = $this->getInput($parsedInput['params'], array('value', 'v', 1));
        // get result message
        $result = $this->addReminder($jabberId, $type, $value);
        break;
      case 'show':
        $page = $this->getInput($parsedInput['params'], array('page', 'p', 0));
        $result = $this->showReminders($jabberId, $page);
        break;
      case 'remove':
      case 'rem':
        $id = $this->getInput($parsedInput['params'], array('id', 'i', 0));
        $result = $this->removeReminder($jabberId, $id);
        break;
      case 'generate':
      case 'gen':
        $result = $this->generateRemindersDates($jabberId);
        break;
      }
    }

    return $result;
  }

  protected function addReminder($jabberId, $type, $value) {
    $added = FALSE;

    if (!empty($jabberId) && !empty($value) &&
        !empty($type) && in_array($type, $this->reminderTypes)) {

      $sql = sprintf (
        "SELECT COUNT(reminder_id) AS amount
           FROM %s
          WHERE reminder_jid = '%s'
            AND reminder_type = '%s'
            AND reminder_value = '%d'",
        $this->dbTableReminders,
        $jabberId,
        $type,
        $value
      );

      if ($res = $this->db->query($sql)) {
        $row = $res->fetch_assoc();
        if ($row !== FALSE && $row['amount'] == 0) {
          $sqlParams = array(
            'reminder_jid' => $jabberId,
            'reminder_type' => $type,
            'reminder_value' => $value
          );
          $sql = sprintf(
            "INSERT INTO %s (%s) 
             VALUES ('%s')",
            $this->dbTableReminders,
            implode(",", array_keys($sqlParams)),
            implode("','", array_values($sqlParams))
          );
          $added = FALSE !== $this->db->query($sql);
        }
      }
    }

    if ($added == TRUE) {
      return 'Your reminder has been added!';
    } else {
      return 'Error, your reminder has not been added!';
    }
  }

  protected function showReminders($jabberId, $page, $limit = 5, $offset = 0) {

    if (!empty($jabberId)) {
      
      $sql = sprintf(
        "SELECT COUNT(*) AS amount
           FROM %s
          WHERE reminder_jid = '%s'",
        $this->dbTableReminders,
        $jabberId
      );
      
      if ($res = $this->db->query($sql)) {
        $row = $res->fetch_assoc();
        if ($row['amount'] > 0) {

          $maxPage = ceil($row['amount'] / $limit);
          if (!empty($page) && $page <= $maxPage) {
            $offset = $page - 1; // get offset by params
          } else {
            $offset = 0;
          }

          $sql = sprintf(
            "SELECT reminder_id, reminder_type, reminder_value
               FROM %s
              WHERE reminder_jid = '%s'
              ORDER BY reminder_type, reminder_value
              LIMIT %s, %s",
            $this->dbTableReminders,
            $jabberId,
            $offset * $limit,
            $limit
          );

          if ($res = $this->db->query($sql)) {
            $msg = "Your reminders are:";
            while ($row = $res->fetch_assoc()) {

              switch ($row['reminder_type']) {
              case 'weeks':
              case 'week':
              case 'w':
                $typeExtension = "week(s)";
                break;
              case 'days':
              case 'day':
              case 'd':
                $typeExtension = "day(s)";
                break;
              case 'hours':
              case 'hour':
              case 'h':
                $typeExtension = "hour(s)";
                break;
              case 'minutes':
              case 'minute':
              case 'm':
                $typeExtension = "minute(s)";
                break;
              default:
                $typeExtension = "";
              }

              $msg .= sprintf(
                "\nRemind me %d %s before date. (ID %d)",
                $row['reminder_value'], $typeExtension, $row['reminder_id']
              );
            }
            if ($maxPage > 1) {
              $msg .= sprintf(
                "\nPage %d of %d, use -page X to change page offset.",
                $offset+1, $maxPage
              );
            }
          }
        }
      }
    }

    if (!empty($msg)) {
      return $msg;
    } else {
      return 'Error, no reminders are available!';
    }
  }

  protected function removeReminder($jabberId, $id) {
    $removed = FALSE;

    if (!empty($jabberId) && !empty($id)) {
      $sql = sprintf(
        "DELETE FROM %s 
          WHERE reminder_id = '%d'
            AND reminder_jid = '%s'",
        $this->dbTableReminders,
        $id,
        $jabberId
      );
      
      $removed = FALSE !== $this->db->query($sql);
    }

    if ($removed == TRUE) {
      return 'Your reminder has been removed!';
    } else {
      return 'Error, your reminder has not been removed!';
    }
  }

  protected function getReminderTimes($jabberId) {
    $reminderTimes = array();

    $sql = sprintf(
      "SELECT reminder_type, reminder_value
         FROM %s
        WHERE reminder_jid = '%s'",
      $this->dbTableReminders,
      $jabberId
    );

    if ($res = $this->db->query($sql)) {
      while ($row = $res->fetch_assoc()) {
        switch ($row['reminder_type']) {
        case 'weeks':
        case 'week':
        case 'w':
          $reminderTimes[] = $row['reminder_value'] * 7 * 24 * 60 * 60;
          break;
        case 'days':
        case 'day':
        case 'd':
          $reminderTimes[] = $row['reminder_value'] * 24 * 60 * 60;
          break;
        case 'hours':
        case 'hour':
        case 'h':
          $reminderTimes[] = $row['reminder_value'] * 60 * 60;
          break;
        case 'minutes':
        case 'minute':
        case 'm':
          $reminderTimes[] = $row['reminder_value'] * 60;
          break;
        default:
        }
      }
    }

    return $reminderTimes;
  }

  protected function generateRemindersDates($jabberId) {
    $generated = FALSE;

    if (!empty($jabberId)) {
      
      $sql = sprintf(
        "DELETE FROM %s 
          WHERE reminder_date_jid = '%s'",
        $this->dbTableRemindersDates,
        $jabberId
      );
      $this->db->query($sql);

      $reminderTimes = $this->getReminderTimes($jabberId);
      if (is_array($reminderTimes) && count($reminderTimes) > 0) {
        $sql = sprintf(
          "SELECT date_id, date_time
             FROM %s
            WHERE date_jid = '%s' AND date_past = '%d'
            GROUP BY date_time",
          $this->dbTableDates,
          $jabberId, 0
        );

        if ($res = $this->db->query($sql)) {
          $inserts = array();
          $currentTime = time();
          $generated = TRUE;
          while ($row = $res->fetch_assoc()) {
            foreach ($reminderTimes as &$reminderTime) {
              $reminderDateTime = $row['date_time'] - $reminderTime;
              if ($reminderDateTime > $currentTime) {
                $inserts[] = array(
                  'reminder_date_id' => $row['date_id'],
                  'reminder_date_time' => $reminderDateTime,
                  'reminder_date_jid' => $jabberId
                );
              }
            }
          }

          if (count($inserts) > 0) {
            $sql = sprintf(
              "INSERT INTO %s (%s) VALUES ", 
              $this->dbTableRemindersDates, 
              implode(",", array_keys($inserts[0]))
            );
            foreach ($inserts as $key => $insert) {
              if ($key > 0) {
                $sql .= ", ";
              }
              $sql .= sprintf(
                "('%s')", 
                implode("','", array_values($insert))
              );
            }
            $generated = FALSE !== $this->db->query($sql);
            unset($inserts);
          }
        }
      }
    }

    if ($generated === TRUE) {
      return 'Your reminders dates have been generated!';
    } else {
      return 'Error, your reminders dates have not been generated!';
    }
  }

  public function update(&$msgs) {
    // send reminder messages
    $currentTime = time();

    $sql = sprintf(
      "SELECT d.date_id, d.date_jid, d.date_time, d.date_note
         FROM %s rd
         JOIN %s d
           ON d.date_id = rd.reminder_date_id AND d.date_past = %d
        WHERE rd.reminder_date_time < %d
        GROUP BY d.date_id
        ORDER BY d.date_time",
      $this->dbTableRemindersDates,
      $this->dbTableDates,
      0, $currentTime
    );

    if ($res = $this->db->query($sql)) {
      $results = array();

      while ($row = $res->fetch_assoc()) {
        if (!isset($results[$row['date_jid']])) {
          $results[$row['date_jid']] = "";
        }
        $results[$row['date_jid']] .= sprintf("\n%s: %s (ID %d)",
          date("Y-m-d, H:i:s", $row['date_time']), $row['date_note'], $row['date_id']
        );
      }

      if (count($results) > 0) {
        foreach ($results as $jid => $text) {
          $msgs[$jid][] = "I would like to remind you of:".$text;
        }
      }
    }

    // delete previous reminders dates times
    $sql = sprintf(
      "DELETE FROM %s 
        WHERE reminder_date_time < %d",
      $this->dbTableRemindersDates, 
      $currentTime
    );
    $this->db->query($sql);
  }

}

?>

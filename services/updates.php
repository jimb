<?php
/**
 * JIMBS (Jabber Instant Messaging Bot Services)
 * Copyright (C) 2010  Martin Kelm
 * This file is part of JIMB.
 *
 * JIMB is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * JIMB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JIMB; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @file       Services updates
 * @package    JIMB
 * @subpackage Services 
 * @author     Martin Kelm <martinkelm@idxsolutions.de>
 * @copyright  2010 Martin Kelm
 */

require_once(dirname(__FILE__).'/database.cfg.php');
require_once(dirname(__FILE__).'/database.class.php');
require_once(dirname(__FILE__).'/inputParser.class.php');
require_once(dirname(__FILE__).'/mods/modDates.php');
require_once(dirname(__FILE__).'/mods/modReminders.php');
require_once(dirname(__FILE__).'/mods/modDictionary.php');

$database = database::getInstance();
$database->setConnectionData(
  $databaseCfg['host'], $databaseCfg['user'], $databaseCfg['pass'], $databaseCfg['db']
);
if ($database->connect()) {
  $inputParser = new inputParser();
  $mods = array();
  $mods['dates'] = new modDates($database);
  $mods['reminders'] = new modReminders($database);
  $mods['dictionary'] = new modDictionary();
  $modKeys = array_keys($mods);

  $msgs = array();
  foreach ($modKeys as $modKey) {
    $mods[$modKey]->update($msgs);
  }
  
  if (is_array($msgs) && count($msgs) > 0) {
    foreach ($msgs as $jid => $jidMsgs) {
      if (is_array($jidMsgs) && count($jidMsgs) > 0) {
        foreach ($jidMsgs as $msg) {
          $sql = sprintf(
            "INSERT INTO queue_out (jid, message) VALUES ('%s', '%s')",
            $jid, $msg
          );
          $database->query($sql);
        }
      }
    }
  }
  
  $database->close();
}

?>
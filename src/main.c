/**
 * JIMCI (Jabber Instant Messaging Connection Interface)
 * This file is part of JIMB.
 *
 * JIMCI is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License Version 3
 * as published by the Free Software Foundation.
 *
 * JIMCI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License Version 3
 * along with JIMCI; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: Main
 * Package: JIMCI
 * Author: Martin Kelm <martinkelm@idxsolutions.de>
 */
#include "main.h"
#include "config.h"
#include "mysql.h"
#include "connection.h"
#include "services.h"

/*
 * Execute program
 */
int main(int argv, char**argc) {
  int check;
  debug("Started");
  // initialize configiuration structures with values
  if (cfg_initialize(0) == 1) {
    // connect to database
    check = mysql_connect(
      cfg_mysql.server, cfg_mysql.user, cfg_mysql.password, cfg_mysql.database
    );
    if (check > 0) {
      services_init(
        cfg_services.actions_exec, cfg_services.actions_script, cfg_services.actions_interval,
        cfg_services.updates_exec, cfg_services.updates_script, cfg_services.updates_interval,
        cfg_services.queue_message
      );
      conn_create(
        cfg_jabber.host, cfg_jabber.port, cfg_jabber.ssl, cfg_jabber.ssl_fingerprint,
        cfg_jabber.user, cfg_jabber.resource, cfg_jabber.password
      );
    }
    // disconnect from database
    mysql_disconnect();
  }
  return 1;
}

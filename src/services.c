/**
 * JIMCI (Jabber Instant Messaging Connection Interface)
 * This file is part of JIMB.
 *
 * JIMCI is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License Version 3
 * as published by the Free Software Foundation.
 *
 * JIMCI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License Version 3
 * along with JIMCI; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: Services
 * Package: JIMCI
 * Author: Martin Kelm <martinkelm@idxsolutions.de>
 */

#include <stdlib.h>
#include <string.h>
#include <glib.h>
#include "main.h"
#include "services.h"

gchar *services_actions_exec;
gchar *services_actions_script;
gchar *services_updates_exec;
gchar *services_updates_script;
extern gchar *services_queue_message;

gboolean services_actions_perform(gpointer data) {
  gchar *command = g_strdup_printf("%s%s", services_actions_exec, services_actions_script);
  system(command);
  debug("Services actions execution: %s", command);
  return TRUE;
}

gboolean services_updates_perform(gpointer data) {
  gchar *command = g_strdup_printf("%s%s", services_updates_exec, services_updates_script);
  system(command);
  debug("Services updates execution: %s", command);
  return TRUE;
}

int services_init(
      char *actions_exec, char *actions_script, int actions_interval, 
      char *updates_exec, char *updates_script, int updates_interval, char *queue_message
    ) {
  if (strlen(queue_message) > 0) {
    services_queue_message = queue_message;
  } else {
    services_queue_message = "";
  }
  if (strlen(actions_exec) > 0 && actions_interval > 0) {
    if (strlen(actions_script) > 0) {
      services_actions_script = g_strdup_printf(" %s", actions_script);
    } else {
      services_actions_script = "";
    }
    services_actions_exec = actions_exec;
    gint func_ref_actions = g_timeout_add(actions_interval * 1000, services_actions_perform, NULL);
  }
  if (strlen(updates_exec) > 0 && updates_interval > 0) {
    if (strlen(updates_script) > 0) {
      services_updates_script = g_strdup_printf(" %s", updates_script);
    } else {
      services_updates_script = "";
    }
    services_updates_exec = updates_exec;
    gint func_ref_updates = g_timeout_add(updates_interval * 1000, services_updates_perform, NULL);
  }
  
  debug("Services initialized");
}
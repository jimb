/**
 * JIMCI (Jabber Instant Messaging Connection Interface)
 * This file is part of JIMB.
 *
 * JIMCI is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License Version 3
 * as published by the Free Software Foundation.
 *
 * JIMCI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License Version 3
 * along with JIMCI; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: Config header
 * Package: JIMCI
 * Author: Martin Kelm <martinkelm@idxsolutions.de>
 */
typedef struct cfg_st_jabber cfg_st_jabber;
struct cfg_st_jabber {
  char host[256];
  int port;
  char user[256];
  char resource[256];
  char password[256];
  int ssl;
  char ssl_fingerprint[256];
} cfg_jabber;

typedef struct cfg_st_mysql cfg_st_mysql;
struct cfg_st_mysql {
  char server[256];
  char user[256];
  char password[256];
  char database[256];
} cfg_mysql;

typedef struct cfg_st_services cfg_st_services;
struct cfg_st_services {
  char actions_exec[256];
  char actions_script[256];
  int actions_interval;
  char updates_exec[256];
  char updates_script[256];
  int updates_interval;
  char queue_message[256];
} cfg_services;

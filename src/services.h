/**
 * JIMCI (Jabber Instant Messaging Connection Interface)
 * This file is part of JIMB.
 *
 * JIMCI is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License Version 3
 * as published by the Free Software Foundation.
 *
 * JIMCI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License Version 3
 * along with JIMCI; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: Services header
 * Package: JIMCI
 * Author: Martin Kelm <martinkelm@idxsolutions.de>
 */

gchar *services_queue_message;

int services_init(
  char *actions_exec, char *actions_script, int actions_interval, 
  char *updates_exec, char *updates_script, int updates_interval, 
  char *queue_message
);

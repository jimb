/**
 * JIMCI (Jabber Instant Messaging Connection Interface)
 * This file is part of JIMB.
 *
 * JIMCI is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License Version 3
 * as published by the Free Software Foundation.
 *
 * JIMCI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License Version 3
 * along with JIMCI; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: Config
 * Package: JIMCI
 * Author: Martin Kelm <martinkelm@idxsolutions.de>
 */
#include <libconfig.h>
#include <string.h>

#include "main.h"
#include "config.h"

extern cfg_st_jabber cfg_jabber;
extern cfg_st_mysql cfg_mysql;
extern cfg_st_services cfg_services;
char cfg_file[256];

/*
 * Set a configuration mode to get a related file by folder or user.
 */
int cfg_set_config_mode(int mode_number) {
  switch (mode_number) {
    case 0: // config by folder
      snprintf(cfg_file, 256, "%s/config", ".");
      return 1;
      break;
    case 1: // config by user('s home dir), not available yet
      // snprintf(cfg_file, 256, "/%s/.cjimb/config", (char*)getenv("HOME"));
      return -1;
      break;
  }
  return -1;
}

/*
 * Get a int value from config for a related configuration structure.
 */
int cfg_get_int_value(const config_t *config, char *name) {
  long result;
  if (strlen(name) > 0) {
    config_lookup_int(config, name, &result);
  }
  if (result >= 0) {
    return (int)result;
  }
  return -1;
}

/*
 * Get a char value from config for a related configuration structure.
 */
char* cfg_get_char_value(const config_t *config, char *name) {
  const char *result;
  if (strlen(name) > 0) {
    config_lookup_string(config, name, &result);
  }
  if (strlen(result) > 0) {
    return (char*)result;
  }
  return NULL;
}

int cfg_get_jabber_values(const config_t *config) {
  char *tmp_char;
  int tmp_int;
  // config value: jabber host
  tmp_char = cfg_get_char_value(config, "jabber_host");
  if (tmp_char != NULL && strlen(tmp_char) > 0 && strlen(tmp_char) <= 256) {
    snprintf(cfg_jabber.host, 256, "%s", tmp_char);
  } else {
    return -1;
  }
  // config value: jabber port
  tmp_int = cfg_get_int_value(config, "jabber_port");
  if (tmp_int != -1) {
    cfg_jabber.port = tmp_int;
  } else {
    return -1;
  }
  // config value: jabber user
  tmp_char = cfg_get_char_value(config, "jabber_user");
  if (tmp_char != NULL && strlen(tmp_char) > 0 && strlen(tmp_char) <= 256) {
    snprintf(cfg_jabber.user, 256, "%s", tmp_char);
  } else {
    return -1;
  }
  // config value: jabber resource
  tmp_char = cfg_get_char_value(config, "jabber_resource");
  if (tmp_char != NULL && strlen(tmp_char) > 0 && strlen(tmp_char) <= 256) {
    snprintf(cfg_jabber.resource, 256, "%s", tmp_char);
  } else {
    return -1;
  }
  // config value: jabber password
  tmp_char = cfg_get_char_value(config, "jabber_password");
  if (tmp_char != NULL && strlen(tmp_char) > 0 && strlen(tmp_char) <= 256) {
    snprintf(cfg_jabber.password, 256, "%s", tmp_char);
  } else {
    return -1;
  }
  // config value: jabber ssl
  tmp_int = cfg_get_int_value(config, "jabber_ssl");
  if (tmp_int != -1) {
    cfg_jabber.ssl = tmp_int;
  } else {
    return -1;
  }
  // config value: jabber ssl fingerprint
  tmp_char = cfg_get_char_value(config, "jabber_ssl_fingerprint");
  if (tmp_char != NULL && strlen(tmp_char) > 0 && strlen(tmp_char) <= 256) {
    snprintf(cfg_jabber.ssl_fingerprint, 256, "%s", tmp_char);
  } else {
    snprintf(cfg_jabber.ssl_fingerprint, 256, "");
  }
  return 1;
}

int cfg_get_mysql_values(const config_t *config) {
  char *tmp_char;
  // config value: mysql server
  tmp_char = cfg_get_char_value(config, "mysql_server");
  if (tmp_char != NULL && strlen(tmp_char) > 0 && strlen(tmp_char) <= 256) {
    snprintf(cfg_mysql.server, 256, "%s", tmp_char);
  } else {
    return -1;
  }
  // config value: mysql user
  tmp_char = cfg_get_char_value(config, "mysql_user");
  if (tmp_char != NULL && strlen(tmp_char) > 0 && strlen(tmp_char) <= 256) {
    snprintf(cfg_mysql.user, 256, "%s", tmp_char);
  } else {
    return -1;
  }
  // config value: mysql password
  tmp_char = cfg_get_char_value(config, "mysql_password");
  if (tmp_char != NULL && strlen(tmp_char) > 0 && strlen(tmp_char) <= 256) {
    snprintf(cfg_mysql.password, 256, "%s", tmp_char);
  } else {
    return -1;
  }
  // config value: mysql database
  tmp_char = cfg_get_char_value(config, "mysql_database");
  if (tmp_char != NULL && strlen(tmp_char) > 0 && strlen(tmp_char) <= 256) {
    snprintf(cfg_mysql.database, 256, "%s", tmp_char);
  } else {
    return -1;
  }
  return 1;
}

int cfg_get_services_values(const config_t *config) {
  char *tmp_char;
  int tmp_int;
  // config value: services actions executable
  tmp_char = cfg_get_char_value(config, "services_actions_exec");
  if (tmp_char != NULL && strlen(tmp_char) > 0 && strlen(tmp_char) <= 256) {
    snprintf(cfg_services.actions_exec, 256, "%s", tmp_char);
  }
  // config value: services actions script (optional)
  tmp_char = cfg_get_char_value(config, "services_actions_script");
  if (tmp_char != NULL && strlen(tmp_char) > 0 && strlen(tmp_char) <= 256) {
    snprintf(cfg_services.actions_script, 256, "%s", tmp_char);
  } else {
    snprintf(cfg_services.actions_script, 256, ""); // default
  }
  // config value: services actions interval (optional)
  tmp_int = cfg_get_int_value(config, "services_actions_interval");
  if (tmp_int > 0) {
    cfg_services.actions_interval = tmp_int;
  } else {
    cfg_services.actions_interval = 3; // default
  }
  // config value: services updates executable
  tmp_char = cfg_get_char_value(config, "services_updates_exec");
  if (tmp_char != NULL && strlen(tmp_char) > 0 && strlen(tmp_char) <= 256) {
    snprintf(cfg_services.updates_exec, 256, "%s", tmp_char);
  }
  // config value: services updates script (optional)
  tmp_char = cfg_get_char_value(config, "services_updates_script");
  if (tmp_char != NULL && strlen(tmp_char) > 0 && strlen(tmp_char) <= 256) {
    snprintf(cfg_services.updates_script, 256, "%s", tmp_char);
  } else {
    snprintf(cfg_services.updates_script, 256, ""); // default
  }
  // config value: services updates interval (optional)
  tmp_int = cfg_get_int_value(config, "services_updates_interval");
  if (tmp_int > 0) {
    cfg_services.updates_interval = tmp_int;
  } else {
    cfg_services.updates_interval = 30; // default
  }
  // config value: services queue message (optional)
  tmp_char = cfg_get_char_value(config, "services_queue_message");
  if (tmp_char != NULL && strlen(tmp_char) > 0 && strlen(tmp_char) <= 256) {
    snprintf(cfg_services.queue_message, 256, "%s", tmp_char);
  } else {
    snprintf(cfg_services.queue_message, 256, ""); // default
  }
  return 1;
}

int cfg_initialize(int mode_number) {
  if (cfg_set_config_mode(mode_number) == 1) {
    config_t config;
    config_init(&config);
    // read config file
    if (config_read_file(&config, cfg_file) == CONFIG_FALSE) {
      fprintf(
        stderr, "Error: Cannot read config file '%s': %s\n",
        cfg_file, config_error_text(&config)
      );
      config_destroy(&config);
      return -1;
    }
    // get jabber config values
    if (cfg_get_jabber_values(&config) == -1) {
      fprintf(
        stderr, "Error: Cannot look up jabber config data in '%s': %s\n",
        cfg_file, config_error_text(&config)
      );
      config_destroy(&config);
      return -1;
    }
    // get mysql config values
    if (cfg_get_mysql_values(&config) == -1) {
      fprintf(
        stderr, "Error: Cannot look up mysql config data in '%s': %s\n",
        cfg_file, config_error_text(&config)
      );
      config_destroy(&config);
      return -1;
    }
    // get services config values
    if (cfg_get_services_values(&config) == -1) {
      fprintf(
        stderr, "Error: Cannot look up services config data in '%s': %s\n",
        cfg_file, config_error_text(&config)
      );
      config_destroy(&config);
      return -1;
    }
    return 1;
  }
  return -1;
}



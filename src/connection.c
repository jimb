/**
 * JIMCI (Jabber Instant Messaging Connection Interface)
 * This file is part of JIMB.
 *
 * JIMCI is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License Version 3
 * as published by the Free Software Foundation.
 *
 * JIMCI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License Version 3
 * along with JIMCI; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: Connection
 * Package: JIMCI
 * Author: Martin Kelm <martinkelm@idxsolutions.de>
 */
#include <stdlib.h>
#include <glib.h>
#include <loudmouth/loudmouth.h>
#include <string.h>

#include "main.h"
#include "connection.h"
#include "services.h"
#include "mysql.h"

LmConnection *conn;
LmSSL *conn_ssl;
GError *error = NULL;
static GMainLoop *main_loop = NULL;
static gboolean online = FALSE;

static gchar *c_host = NULL;
static gint c_port = 0;
static gchar *c_user = NULL;
static gchar *c_password = NULL;
static gchar *c_resource = "";

//static const USE_TCP_KEEPALIVES = 1;

static void conn_auth_cb(
    LmConnection *connection, gboolean success, gpointer user_data
  ) {
  if (success) {
    online = TRUE;

    LmMessage *m;
    m = lm_message_new_with_sub_type(
      NULL, LM_MESSAGE_TYPE_PRESENCE, LM_MESSAGE_SUB_TYPE_AVAILABLE
    );
    lm_connection_send(conn, m, NULL);
    lm_message_unref(m);
    debug("Authentificated with: %s", c_user);
  } else {
    g_print("Couldn't authenticate with '%s':\n%s\n", c_user, error->message);
    g_free(error);
    g_main_loop_quit(main_loop);
  }
}

static void conn_open_cb (
    LmConnection *connection, gboolean success, gpointer user_data
  ) {
  if (success) {
    lm_connection_authenticate(
      conn, c_user, c_password, c_resource, conn_auth_cb, NULL, FALSE, NULL
    );
    debug("Connected to: %s", c_host);
  } else {
    g_printerr("Failed to connect to %s: %s\n", c_host, error->message);
    g_free(error);
    g_main_loop_quit(main_loop);
  }
}

static void conn_close_cb (
    LmConnection *connection, LmDisconnectReason reason, gpointer user_data
  ) {
  lm_connection_close(conn, NULL);
  lm_connection_unref(conn);

  online = FALSE;

  const char *str;
  switch (reason) {
  case LM_DISCONNECT_REASON_OK:
    str = "LM_DISCONNECT_REASON_OK";
    break;
  case LM_DISCONNECT_REASON_PING_TIME_OUT:
    str = "LM_DISCONNECT_REASON_PING_TIME_OUT";
    break;
  case LM_DISCONNECT_REASON_HUP:
    str = "LM_DISCONNECT_REASON_HUP";
    break;
  case LM_DISCONNECT_REASON_ERROR:
    str = "LM_DISCONNECT_REASON_ERROR";
    break;
  case LM_DISCONNECT_REASON_UNKNOWN:
  default:
    str = "LM_DISCONNECT_REASON_UNKNOWN";
    break;
  }

  g_print("Disconnected, reason %d: %s\n", reason, str);
  lm_ssl_unref(conn_ssl);
  g_main_loop_quit(main_loop);
}

static LmHandlerResult conn_handle_messages(
    LmMessageHandler *handler, LmConnection *connection,
    LmMessage *m, gpointer user_data
  ) {
  LmMessageNode *msgBody = lm_message_node_get_child(m->node, "body");
  const gchar *msgFromVal = lm_message_node_get_attribute(m->node, "from");
  const gchar *msgSelfVal = g_strdup_printf("%s@%s/%s", c_user, c_host, c_resource);

  debug("lm handler result");
  if (strlen(msgFromVal) > 0 && strcmp(msgFromVal, msgSelfVal) == 0) {
    debug("Loopback message from %s.", msgFromVal);
    lm_message_unref(m);

  } else if (strlen(msgFromVal) > 0 && msgBody) {
    const gchar *msgBodyVal = lm_message_node_get_value(msgBody);
    if (strlen(msgBodyVal) > 0) {
      g_print("Incoming message %s from %s\n", msgBodyVal, msgFromVal);

      gchar *valuesSql;
      valuesSql = g_strdup_printf(
        "NULL, '%s', '%s', NULL, NULL, NULL", msgFromVal, msgBodyVal);
      if (mysql_insert_values("queue_in", valuesSql) > 0 &&
          strlen(services_queue_message) > 0) {

        m = lm_message_new(msgFromVal, LM_MESSAGE_TYPE_MESSAGE);
        lm_message_node_add_child(m->node, "body", services_queue_message);

        if (!lm_connection_send(connection, m, &error)) {
          g_print("Error while sending message to '%s':\n%s\n",
                  msgFromVal, error->message);
          g_free(error);
        }
        lm_message_unref(m);
      }
      g_free(valuesSql);
    }
  }
  return LM_HANDLER_RESULT_REMOVE_MESSAGE;
}

gboolean conn_send_messages(gpointer data) {
  debug("conn send messages --- %d : %d", lm_connection_get_state(conn), LM_CONNECTION_STATE_AUTHENTICATED);
  if (lm_connection_get_state(conn) == LM_CONNECTION_STATE_AUTHENTICATED) {
    int i, j, fieldsByRow, fieldsIndex;
    gchar *to, *msg;
    LmMessage *m;
    debug("try to fetch outgoing messages");
    gboolean fetch = mysql_fetch_rows("queue_out", "jid, message");
    if (fetch == TRUE && mysqlIndexRows->len > 0 && mysqlFields->len > 0) {
      fieldsByRow = mysqlFields->len / mysqlIndexRows->len;
      debug("go through entries");
      for (i = 0; i < mysqlIndexRows->len; i++) {

        fieldsIndex = g_array_index(mysqlIndexRows, int, i);
        for (j = 0; j < fieldsByRow; j++) {
          if (j == 0) {
            to = g_strdup_printf(
              "%s", g_array_index(mysqlFields, gchar *, fieldsIndex+j)
            );
          } else {
            msg = g_strdup_printf(
              "%s", g_array_index(mysqlFields, gchar *, fieldsIndex+j)
            );
          }
        }
        if (strlen(to) > 0 && strlen(msg) > 0) {
          m = lm_message_new(to, LM_MESSAGE_TYPE_MESSAGE);
          lm_message_node_add_child(m->node, "body", msg);
          g_print("Outgoing message %s to %s\n", msg, to);
          if (!lm_connection_send(conn, m, &error)) {
            g_print("Error while sending message to '%s':\n%s\n",
                    to, error->message);
            g_free(error);
          }
          lm_message_unref(m);
        }
      }
      g_free(to);
      g_free(msg);
      mysql_trunk_table("queue_out");
    }
    return TRUE;
  }
  return FALSE;
}

gboolean conn_send_loopback_message(gpointer data) {
  debug("conn send loopback message");
  if (lm_connection_get_state(conn) == LM_CONNECTION_STATE_AUTHENTICATED) {
    LmMessage *m;
    if (strlen(c_user) > 0) {
      m = lm_message_new(
        g_strdup_printf("%s@%s/%s", c_user, c_host, c_resource),
        LM_MESSAGE_TYPE_MESSAGE
      );
      if (!lm_connection_send(conn, m, &error)) {
        g_print("Error while sending loopback message:\n%s\n", error->message);
        g_free(error);
      } else {
        debug("Send loopback message to own user.");
      }
      lm_message_unref(m);
    }
    return TRUE;
  }
  return FALSE;
}

static LmSSLResponse conn_ssl_response(LmSSL *ssl, LmSSLStatus status, gpointer data) {
  g_print("SSL Status: %d -> ", status);

  switch (status) {
  case LM_SSL_STATUS_NO_CERT_FOUND:
    g_print("No certificate found!\n");
    break;
  case LM_SSL_STATUS_UNTRUSTED_CERT:
    g_print("Certificate is not trusted!\n");
    break;
  case LM_SSL_STATUS_CERT_EXPIRED:
    g_print("Certificate has expired!\n");
    break;
  case LM_SSL_STATUS_CERT_NOT_ACTIVATED:
    g_print("Certificate has not been activated!\n");
    break;
  case LM_SSL_STATUS_CERT_HOSTNAME_MISMATCH:
    g_print("Certificate hostname does not match expected hostname!\n");
    break;
  case LM_SSL_STATUS_CERT_FINGERPRINT_MISMATCH:
    g_print("Certificate fingerprint does not match expected fingerprint!\n");
    char fpr[59];
    fingerprint_to_hex((const unsigned char*)lm_ssl_get_fingerprint(ssl), fpr);
    g_print("Remote fingerprint: %s\n", fpr);
    break;
  case LM_SSL_STATUS_GENERIC_ERROR:
    g_print("Generic SSL error!\n");
    break;
  }

  return LM_SSL_RESPONSE_CONTINUE;
}

void fingerprint_to_hex(const unsigned char *fpr, char hex[59]) {
  int i;
  char *p;

  for (p = hex, i = 0; i < 15; i++, p+=3) {
    g_sprintf(p, "%02X:", fpr[i]);
    g_sprintf(p, "%02X", fpr[i]);
    hex[58] = '\0';
  }
}

gboolean hex_to_fingerprint(const char *hex, char fpr[16]) {
  int i;
  char *p;

  g_print("Input fingerprint: %s (%d)\n", hex, strlen(hex));
  if (strlen(hex) != 59) return FALSE;
  for (i = 0, p = (char*)hex; *p && *(p+1); i++, p += 3) {
    fpr[i] = (char) g_ascii_strtoull (p, NULL, 16);
  }
  return TRUE;
}

int conn_create(char *host, int port, int ssl, char *ssl_fingerprint,
                char *user, char *resource, char *password) {
  c_host = host;
  c_port = port;
  c_user = user;
  c_resource = resource;
  c_password = password;

  debug("Set lm connection and connection data");
  gboolean result;
  conn = lm_connection_new(c_host);
  if (ssl > 0) {
    char fingerprint[16];
    if (strlen(ssl_fingerprint) > 0 && !hex_to_fingerprint(ssl_fingerprint, fingerprint)) {
      debug("Failed to get valid fingerprint");
      return;
    }
    conn_ssl = lm_ssl_new(
      strlen(ssl_fingerprint) > 0 ? fingerprint : NULL,
      conn_ssl_response,
      NULL,
      NULL
    );
    if (conn_ssl) {
      lm_connection_set_ssl(conn, conn_ssl);
      lm_ssl_unref(conn_ssl);
    } else {
      debug("Failed to create ssl connection");
    }
  }

  lm_connection_set_port(conn, c_port);
  LmMessageHandler *handler;

  gchar *jid;
  jid = g_strdup_printf("%s@%s", c_user, c_host);
  lm_connection_set_jid(conn, jid);
  g_free(jid);

  debug("Set lm messages handler");
  handler = lm_message_handler_new(conn_handle_messages, NULL, NULL);
  lm_connection_register_message_handler(
    conn, handler, LM_MESSAGE_TYPE_MESSAGE, LM_HANDLER_PRIORITY_NORMAL
  );
  lm_message_handler_unref(handler);

  debug("Set lm disconnect function");
  lm_connection_set_disconnect_function(conn, conn_close_cb, NULL, NULL);

  debug("Try to open lm connection");
  result = lm_connection_open(
    conn, (LmResultFunction)conn_open_cb, NULL, NULL, &error
  );
  if (!result) {
    g_print("Couldn't open connection to '%s':\n%s\n", host, error->message);
    g_free(error);
    return -1;
  }
  debug("Lm connection established");

  debug("Set send messages / loopback messages timeouts and main loop");
  gint func_ref = g_timeout_add(1000, conn_send_messages, NULL);
  gint func_loopback_ref = g_timeout_add(30000, conn_send_loopback_message, NULL);
  lm_connection_set_keep_alive_rate(conn, 30);

  main_loop = g_main_loop_new(NULL, FALSE);
  g_main_loop_run(main_loop);
}

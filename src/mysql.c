/**
 * JIMCI (Jabber Instant Messaging Connection Interface)
 * This file is part of JIMB.
 *
 * JIMCI is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License Version 3
 * as published by the Free Software Foundation.
 *
 * JIMCI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License Version 3
 * along with JIMCI; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: MySQL connection
 * Package: JIMCI
 * Author: Martin Kelm <martinkelm@idxsolutions.de>
 */
#include <my_global.h>
#include <my_sys.h>
#include <mysql.h>
#include <glib.h>

#include "main.h"
#include "mysql.h"

static MYSQL *conn; // pointer to connection handler

extern GArray *mysqlFields;
extern GArray *mysqlIndexRows;

/*
 * Print mysql specific error message.
 */
void mysql_print_error(MYSQL *conn, char *message) {
  fprintf(stderr, "%s\n", message);
  if(conn != NULL) {
#if MYSQL_VERSION_ID >= 40101
    fprintf(
      stderr, "Error %u (%s): %s\n",
      mysql_errno(conn), mysql_sqlstate(conn), mysql_error(conn)
    );
#else
    fprintf(
      stderr, "Error %u: %s\n",
      mysql_errno(conn), mysql_error(conn)
    );
#endif
  }
}

/*
 * Insert values in a specified mysql table.
 */
int mysql_insert_values(const gchar *table, const gchar *values) {
  gchar *sql;
  sql = g_strdup_printf("INSERT INTO %s VALUES (%s);", table, values);

  if (mysql_query(conn, sql) != 0) {
    mysql_print_error(conn, "INSERT statement failed.\n");
    return -1;
  }
  g_free(sql);
  return 1;
}

/*
 * Trunk a specified mysql table.
 */
int mysql_trunk_table(const gchar *table) {
  gchar *sql;
  sql = g_strdup_printf("TRUNCATE TABLE %s;", table);

  if (mysql_query(conn, sql) != 0) {
    mysql_print_error(conn, "TRUNK statement failed.\n");
    return -1;
  }
  g_free(sql);
  return 1;
}

/*
 * Get rows in a specified mysql table.
 */
gboolean mysql_fetch_rows(const gchar *table, const gchar *columns) {
  gboolean result = FALSE;
  MYSQL_RES *res_set;
  MYSQL_ROW row;
  int i, fi;
  gchar *sql;

  sql = g_strdup_printf("SELECT %s FROM %s;", columns, table);
  if (mysql_query(conn, sql) != 0)  {
    g_free(sql);
    mysql_print_error(conn, "SELECT statement failed.\n");
    return;
  }
  g_free(sql);

  if (mysqlIndexRows->len > 0) {
    g_array_remove_range(mysqlIndexRows, 0, mysqlIndexRows->len);
  }
  if (mysqlFields->len > 0) {
    g_array_remove_range(mysqlFields, 0, mysqlFields->len);
  }

  res_set = mysql_store_result(conn);
  if (res_set) {
    if ((int)mysql_affected_rows(conn) > 0) {
      fi = 0;
      while ((row = mysql_fetch_row(res_set)) != NULL) {
        mysql_field_seek(res_set, 0);
        g_array_append_val(mysqlIndexRows, fi);
        for (i = 0; i < mysql_num_fields(res_set); i++) {
          g_array_append_val(mysqlFields, row[i]);
          fi++;
        }
      }
      g_free(row);
    }
    result = TRUE;
  } else {
    mysql_print_error(conn, "Could not get result set.\n");
  }
  mysql_free_result(res_set);

  return result;
}

/*
 * Connect to mysql database server by given parameters.
 */
int mysql_connect(char *server, char *user, char *password, char *database) {
  unsigned int port = 3306; // port number (use default value)
  char *socket_name = NULL; // socket name (use built-in value)
  unsigned int flags = 0; // connection flags (none)
  // initialize connection handler
  conn = mysql_init(NULL);
  if (conn == NULL) {
    fprintf(stderr, "Error: Failed to initialize database.\n");
    return -1;
  }
  /* connect to server */
  if (mysql_real_connect(
        conn, server, user, password, database, port, socket_name, flags
      ) == NULL) {
    mysql_print_error(conn, "Error: Cannot connect to database.");
    mysql_close(conn);
    return -1;
  }

  mysqlFields = g_array_new(FALSE, FALSE, sizeof(gchar *));
  mysqlIndexRows = g_array_new(FALSE, FALSE, sizeof(int));
  
  debug("MySQL connection established");
  return 1;
}

/*
 * Disconnect from mysql database server.
 */
int mysql_disconnect() {
  if (conn == NULL) {
    return -1;
  }
  mysql_close(conn);
  
  debug("MySQL disconnected");
  return 1;
}

#!/bin/sh
#
# usage: retrieve-fingerprint.sh remote.host.name [port]
#
REMHOST=$1
REMPORT=${2:-443}

echo |\
./retrieve-cert.sh ${REMHOST}:${REMPORT} | openssl x509 -noout -fingerprint
